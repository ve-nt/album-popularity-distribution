CREATE TABLE albums (
    id char(22) NOT NULL PRIMARY KEY,
    name text NOT NULL,
    album_type varchar(11) NOT NULL,
    popularity integer NOT NULL,
    release_date varchar(10) NOT NULL
);

CREATE TABLE tracks (
    id char(22) NOT NULL PRIMARY KEY,
    album_id char(12) NOT NULL,
    name text NOT NULL,
    track_number integer NOT NULL,
    popularity integer NOT NULL,
    duration bigint NOT NULL,
    explicit boolean NOT NULL
);

CREATE TABLE artists (
    id char(22) NOT NULL PRIMARY KEY,
    name text NOT NULL,
    popularity integer NOT NULL
);

CREATE TABLE artist_albums (
    artist_id char(22) NOT NULL PRIMARY KEY,
    album_id char(12) NOT NULL
);

CREATE TABLE images (
    id char(12) NOT NULL,
    object_type varchar(6) NOT NULL,
    url text NOT NULL,
    height integer NOT NULL,
    width integer NOT NULL,
    PRIMARY KEY (id, object_type)
)
