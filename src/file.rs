use std::fs::File;
use std::io::BufReader;
use std::io::Read;
use std::path::Path;

/// Check if the file exists. If it does exist, return it. If it does
/// not exist, stop the program and exit with an error code.
pub fn open(path: &Path) -> File {
    match File::open(&path) {
        Ok(file) => file,
        Err(error) => {
            eprintln!("{}: {}: {}", crate::PKG_NAME, path.to_str().unwrap(), error);
            std::process::exit(error.raw_os_error().unwrap());
        }
    }
}

pub fn read(file: File) -> String {
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();

    match buf_reader.read_to_string(&mut contents) {
        Ok(size) => size,
        Err(error) => {
            eprintln!("{}: {}", crate::PKG_NAME, error);
            std::process::exit(error.raw_os_error().unwrap());
        }
    };
    contents
}
