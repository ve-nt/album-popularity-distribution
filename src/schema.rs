table! {
    albums (id) {
        id -> Bpchar,
        name -> Text,
        album_type -> Varchar,
        popularity -> Int4,
        release_date -> Varchar,
    }
}

table! {
    artist_albums (artist_id) {
        artist_id -> Bpchar,
        album_id -> Bpchar,
    }
}

table! {
    artists (id) {
        id -> Bpchar,
        name -> Text,
        popularity -> Int4,
    }
}

table! {
    images (id, object_type) {
        id -> Bpchar,
        object_type -> Varchar,
        url -> Text,
        height -> Int4,
        width -> Int4,
    }
}

table! {
    tracks (id) {
        id -> Bpchar,
        album_id -> Bpchar,
        name -> Text,
        track_number -> Int4,
        popularity -> Int4,
        duration -> Int8,
        explicit -> Bool,
    }
}

allow_tables_to_appear_in_same_query!(albums, artist_albums, artists, images, tracks,);
