use num::{FromPrimitive, ToPrimitive};
use std::iter::Sum;
use std::ops::Sub;

/// Returns the larger of two numbers.
pub fn _larger<T>(a: T, b: T) -> T
where
    T: PartialOrd,
{
    if a > b {
        a
    } else {
        b
    }
}

/// Returns the smaller of two numbers.
pub fn _smaller<T>(a: T, b: T) -> T
where
    T: PartialOrd,
{
    if a > b {
        a
    } else {
        b
    }
}

/// Returns the difference between two numbers.
pub fn difference<T>(a: T, b: T) -> T::Output
where
    T: PartialOrd + Sub,
{
    if a > b {
        a - b
    } else {
        b - a
    }
}

/// Returns the largest number in a Vector.
pub fn _largest<T>(set: Vec<T>) -> T
where
    T: PartialOrd + Copy,
{
    let mut largest: T = set[0];
    for n in set {
        if n > largest {
            largest = n;
        }
    }
    largest
}

/// Returns the mean of a list of numbers. Will return None if the set
/// has no elements to avoid dividing by zero.
pub fn mean<'a, T: 'a>(set: &'a [T]) -> Option<f64>
where
    T: ToPrimitive + Sum<&'a T>,
{
    if let 0 = set.len() {
        None
    } else {
        let sum = set.iter().sum::<T>();
        FromPrimitive::from_usize(set.len())
            .and_then(|length: f64| T::to_f64(&sum).map(|val| val / length))
    }
}
