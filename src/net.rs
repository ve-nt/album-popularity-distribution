use crate::spotify;
use reqwest::header::{AUTHORIZATION, CONTENT_TYPE};
use reqwest::{Error, Response};

/// Use our `client_id` and `client_secret` and send them to Spotify. We will
/// recieve back our `access_token`, `token_type`, `scope` and how long until
/// the token expires as `expires_in`. The body will be in JSON format and can
/// be used to serialize into a `ClientCredentials` struct.
pub fn request_spotify_auth(client_id: &str, client_secret: &str) -> Response {
    let authorization: String = format!(
        "{} {}",
        "Basic",
        base64::encode(&format!("{}:{}", client_id, client_secret))
    );

    let response = reqwest::Client::new()
        .post("https://accounts.spotify.com/api/token")
        .header(AUTHORIZATION, authorization)
        .header(CONTENT_TYPE, "application/x-www-form-urlencoded")
        .body("grant_type=client_credentials")
        .send();

    response.unwrap_or_else(|e| {
        reqwest_error_handler(&e);
        std::process::exit(1)
    })
}

/// Get an album's data back from Spotify, query it by `id`. Response will be in
/// JSON format and can be used to serialize into an `Album` type. Note that the
/// `Track` object within this is not a full object, some fields will be
/// missing. For a full `Track` object a track must be queried directly.
pub fn get_album(id: &str, auth: &spotify::ClientCredentials) -> Response {
    let url: String = format!("https://api.spotify.com/v1/albums/{}", id);
    get_spotify_data_response(&url, auth)
}

/// Get an `offset` list of albums from any year, the number of albums being
/// `limit`.
pub fn get_offset_albums(offset: u32, limit: u128, auth: &spotify::ClientCredentials) -> Response {
    let url: String = format!("https://api.spotify.com/v1/search?\
                               q=year:{year}\
                               &type={type}&\
                               limit={limit}&\
                               offset={offset}",
                              year="0000-9999", type="album", limit=limit, offset=offset);

    get_spotify_data_response(&url, auth)
}

/// Get albums from any `year`. This is used to get the total amount of albums
/// in the response, which is needed to get a random album.
pub fn get_albums(auth: &spotify::ClientCredentials) -> Response {
    let url: String = format!("https://api.spotify.com/v1/search?\
                               q=year:{year}\
                               &type={type}",
                              year="0000-9999", type="album");

    get_spotify_data_response(&url, auth)
}

/// Get a track's data back from Spotify. Query is by `id`. Response will be in
/// JSON format and can be used to serialize into an `Track` type.
pub fn get_track(id: &str, auth: &spotify::ClientCredentials) -> Response {
    let url: String = format!("https://api.spotify.com/v1/tracks/{}", id);
    get_spotify_data_response(&url, auth)
}

/// Get an artist's data back from Spotify. Query is by `id`. Response will be
/// in JSON format and can be used to serialize into an `Artist` type.
pub fn get_artist(id: &str, auth: &spotify::ClientCredentials) -> Response {
    let url: String = format!("https://api.spotify.com/v1/artists/{}", id);
    get_spotify_data_response(&url, auth)
}

fn get_spotify_data_response(url: &str, auth: &spotify::ClientCredentials) -> Response {
    let authorization = format!("Bearer {}", auth.access_token);

    let response = reqwest::Client::new()
        .get(url)
        .header(AUTHORIZATION, authorization)
        .send();

    response.unwrap_or_else(|e| {
        reqwest_error_handler(&e);
        std::process::exit(1);
    })
}

fn reqwest_error_handler(e: &Error) {
    eprint!("{}: Request error: ", crate::PKG_NAME);

    if let Some(status) = e.status() {
        eprint!("status code {}: ", status);
    }

    if e.is_http() {
        if let Some(url) = e.url() {
            eprintln!("Problem making request to: {}", url);
        }
    }

    if e.is_redirect() {
        if let Some(redirect_stop) = e.url() {
            eprintln!("redirect loop at {}", redirect_stop);
        } else {
            eprintln!("server redirecting too many times or making loop");
        }
    }

    if e.is_client_error() {
        eprintln!("Client failed to authorize");
    }

    if e.is_timeout() {
        eprintln!("Session timed out");
    }

    if e.is_server_error() {
        eprintln!("Internal server error");
    }
}
