#![feature(rustc_private)]
#![warn(clippy::all, clippy::pedantic)]

#[macro_use]
extern crate diesel;

mod calc;
mod config;
mod database;
mod file;
mod models;
mod net;
mod schema;
mod spotify;

use spotify::Album;
use spotify::AlbumStat;
use spotify::ClientCredentials;

// Some metadata about the package
const PKG_NAME: &str = env!("CARGO_PKG_NAME");

fn main() {
    // Read the user's config file and load into `config`.
    let config = config::Config::new();

    // Authorize with Spotify using the client_id and client_secret from the config file.
    let auth = ClientCredentials::new(&config.keys.client_id, &config.keys.client_secret);

    loop {
        // Get a random album.
        let album = Album::new_random(&auth);

        // Extract the needed data from the album to calculate stats.
        let album_stat = AlbumStat::from(&album);

        // Print the album's information to stdout.
        spotify::print_album_data(&album, &album_stat);
    }
}
