use crate::schema::*;

#[derive(Insertable)]
#[table_name = "albums"]
pub struct NewAlbum {
    pub id: String,
    pub name: String,
    pub album_type: String,
    pub popularity: i32,
    pub release_date: String,
}

#[derive(Queryable)]
pub struct Album {
    pub id: String,
    pub name: String,
    pub album_type: String,
    pub popularity: i32,
    pub release_date: String,
}

#[derive(Insertable)]
#[table_name = "tracks"]
pub struct NewTrack {
    pub id: String,
    pub album_id: String,
    pub name: String,
    pub track_number: i32,
    pub popularity: i32,
    pub duration: i64,
    pub explicit: bool,
}

#[derive(Queryable)]
pub struct Track {
    pub id: String,
    pub album_id: String,
    pub name: String,
    pub track_number: i32,
    pub popularity: i32,
    pub duration: i64,
    pub explicit: bool,
}

#[derive(Insertable)]
#[table_name = "artists"]
pub struct NewArtist {
    pub id: String,
    pub name: String,
    pub popularity: i32,
}

#[derive(Queryable)]
pub struct Artist {
    pub id: String,
    pub name: String,
    pub popularity: i32,
}

#[derive(Queryable)]
pub struct ArtistAlbum {
    pub artist_id: String,
    pub album_id: String,
}

#[derive(Queryable)]
pub struct Image {
    pub id: String,
    pub object_type: String,
    pub url: String,
    pub height: i32,
    pub width: i32,
}
