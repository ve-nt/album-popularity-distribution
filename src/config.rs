use serde::Deserialize;
use std::path::Path;

const DEFAULT_CONFIG_FILE_PATH: &str = "config.toml";

#[derive(Deserialize)]
pub struct Config {
    pub keys: Keys,
}

#[derive(Deserialize)]
pub struct Keys {
    pub client_id: String,
    pub client_secret: String,
}

impl Config {
    pub fn new() -> Self {
        let config_path = Path::new(DEFAULT_CONFIG_FILE_PATH);
        let config_file = crate::file::open(&config_path);
        toml::from_str(&crate::file::read(config_file)).unwrap()
    }
}
