use crate::models::*;
use crate::spotify;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenv::dotenv;
use std::env;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

pub fn insert_album(conn: &PgConnection, album: spotify::Album) -> Album {
    use crate::schema::albums;

    let album_type = match album.album_type {
        spotify::AlbumType::Album => String::from("album"),
        spotify::AlbumType::Single => String::from("single"),
        spotify::AlbumType::Compilation => String::from("compilation"),
    };

    let new_album = NewAlbum {
        id: album.id,
        name: album.name,
        popularity: i32::from(album.popularity),
        release_date: album.release_date,
        album_type,
    };

    diesel::insert_into(albums::table)
        .values(&new_album)
        .get_result(conn)
        .expect("Error inserting new album into database")
}

pub fn insert_track(conn: &PgConnection, track: spotify::Track, album: spotify::Album) -> Track {
    use crate::schema::tracks;

    let new_track = NewTrack {
        id: track.id,
        album_id: album.id,
        name: track.name,
        track_number: i32::from(track.track_number),
        popularity: i32::from(track.popularity.unwrap()),
        duration: i64::from(track.duration_ms),
        explicit: track.explicit,
    };

    diesel::insert_into(tracks::table)
        .values(&new_track)
        .get_result(conn)
        .expect("Error inserting new track into database")
}

pub fn insert_artist(conn: &PgConnection, artist: spotify::Artist) -> Artist {
    use crate::schema::artists;

    let new_artist = NewArtist {
        id: artist.id,
        name: artist.name,
        popularity: i32::from(artist.popularity.unwrap()),
    };

    diesel::insert_into(artists::table)
        .values(&new_artist)
        .get_result(conn)
        .expect("Error inserting new artist into database")
}
