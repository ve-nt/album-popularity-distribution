use crate::calc::{difference, mean};
use rand::Rng;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug)]
enum Token {
    Bearer,
    Basic,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ClientCredentials {
    pub access_token: String,
    token_type: Token,
    scope: String,
    expires_in: u32,
}

impl ClientCredentials {
    pub fn new(client_id: &str, client_secret: &str) -> Self {
        let response = crate::net::request_spotify_auth(client_id, client_secret);
        let handled_response = Self::handled_auth_response(response);

        serde_json::from_value(handled_response)
            .expect("Could not construct client credential's response")
    }

    fn handled_auth_response(mut auth_response: reqwest::Response) -> Value {
        // Get the response from Spotify when requesting client credentials.
        let response_text = auth_response
            .text()
            .expect("Couldn't convert the client credentials response into text!");

        // Check if the response is valid JSON.
        let response_json: HashMap<String, Value> =
            serde_json::from_str(&response_text).expect("Did not get valid JSON from Spotify!");

        // Check if the response was an error. If it is, `error` should be one of the keys.
        if response_json.contains_key("error") && response_json.contains_key("error_description") {
            eprintln!(
                "{}: Error from Spotify: {}",
                crate::PKG_NAME,
                response_json.get("error_description").unwrap()
            );
            std::process::exit(1);
        }

        serde_json::to_value(response_json).unwrap()
    }
}

/// A paging object returned by spotify, which will wrap the response when
/// requesting multiple objects.
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Paging<T> {
    pub items: Vec<T>,
    next: Option<String>,
    previous: Option<String>,
    href: String,
    total: u32,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Artist {
    pub id: String,
    pub name: String,
    pub popularity: Option<u8>,
    href: String,
    uri: String,
}

impl Artist {
    fn new(id: &str, auth: &ClientCredentials) -> Self {
        handle_json(&mut crate::net::get_artist(id, &auth))
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
struct Image {
    height: u32,
    width: u32,
    url: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum AlbumType {
    #[serde(rename = "single")]
    Single,
    #[serde(rename = "album")]
    Album,
    #[serde(rename = "compilation")]
    Compilation,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Track {
    pub id: String,
    pub name: String,
    pub popularity: Option<u8>,
    artists: Vec<Artist>,
    pub duration_ms: u32,
    pub explicit: bool,
    pub track_number: u16,
}

impl Track {
    fn new(id: &str, auth: &ClientCredentials) -> Self {
        handle_json(&mut crate::net::get_track(id, &auth))
    }
}

/// The minimum required fields from a query to an `Album`. This is used when
/// requesting many albums, since some fields will be missing from Spotify's
/// response. All we need is the `id` of the album, then we can make another
/// request to get all the information we need.
#[derive(Serialize, Deserialize, Debug, PartialEq)]
struct AlbumId {
    id: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
struct Albums {
    albums: Paging<AlbumId>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Album {
    pub id: String,
    pub tracks: Paging<Track>,
    pub name: String,
    pub popularity: u8,
    pub album_type: AlbumType,
    pub release_date: String,
    pub artists: Vec<Artist>,
    genres: Vec<String>,
    images: Vec<Image>,
}

impl Album {
    pub fn new(id: &str, auth: &ClientCredentials) -> Self {
        let mut album: Self = handle_json(&mut crate::net::get_album(id, &auth));

        // Gather the data for each track from the album. We need to do this,
        // because the Track objects that spotify responds with as part of the
        // Album will have their popularity missing
        let mut all_track_info: Vec<Track> = Vec::new();
        for track in &album.tracks.items {
            all_track_info.push(Track::new(&track.id, &auth));
        }

        // Replace the track objects with the full track objects.
        album.tracks.items = all_track_info;

        // Gather the data for each artist from the album, since the popularity
        // fields will also be missing from the ones that come with the album.
        let mut all_artist_info: Vec<Artist> = Vec::new();
        for artist in &album.artists {
            all_artist_info.push(Artist::new(&artist.id, &auth));
        }

        // Replace the artists vector with the full artist objects
        album.artists = all_artist_info;

        album
    }

    pub fn new_random(auth: &ClientCredentials) -> Self {
        let several_albums: Albums = handle_json(&mut crate::net::get_albums(&auth));
        let offset: u32 = rand::thread_rng().gen_range(1, several_albums.albums.total);
        let random_album: Albums =
            handle_json(&mut crate::net::get_offset_albums(offset, 1, &auth));

        Self::new(&random_album.albums.items[0].id, &auth)
    }
}

/// Contains all the data that is needed to perform statistics on an `Album`.
/// The reason this is a seperate struct is so that all the data can be aquired
/// in a minimal amount of requests to Spotify.
///
/// This will also hold all the methods for calculating the stats of an `Album`.
pub struct AlbumStat {
    track_popularities: Vec<u32>,
}

impl AlbumStat {
    /// Extracts the data from an `album` into an `AlbumStat` struct.
    pub fn from(album: &Album) -> Self {
        let mut track_popularities = Vec::new();
        for track in &album.tracks.items {
            track_popularities.push(u32::from(track.popularity.unwrap()));
        }

        Self { track_popularities }
    }

    /// Returns the mean of the popularities for each of the tracks on the album.
    pub fn tracks_mean_popularity(&self) -> Option<f64> {
        mean(&self.track_popularities)
    }

    /// Returns the popularity distribution. The mean average of the absolute difference between
    /// each track's popularity and the average popularity of the album.
    pub fn tracks_popularity_distribution(&self) -> Option<f64> {
        // If the album has 0 or 1 tracks, the popularity distribution doesn't apply.
        if self.track_popularities.len() < 2 {
            return None;
        }

        // calculate the mean popularity.
        let mean_popularity = self.tracks_mean_popularity()?;

        let mut popularity_differences = Vec::new();
        for p in &self.track_popularities {
            popularity_differences.push(difference(f64::from(*p), mean_popularity));
        }
        Some(mean(&popularity_differences)?)
    }
}

/// Prints out an album's information in a tree-like structure using box drawing
/// characters. Prints to standard out.
pub fn print_album_data(album: &Album, album_stat: &AlbumStat) {
    // Declare the unicode box characters. These will be used to print a nice
    // output of tracks. The reason we declare these using their code-point is
    // because some (albeit ancient) machines may not display these characters
    // properly when viewing this source.
    let box_drawing_light_vertical = String::from("\u{2502}");
    let box_drawing_light_vertical_and_right = String::from("\u{251C}");
    let box_drawing_light_up_and_right = String::from("\u{2514}");

    // Stringify the album_type enum
    let album_type = match album.album_type {
        AlbumType::Album => "album",
        AlbumType::Compilation => "compilation",
        AlbumType::Single => "single",
    };

    // Generate the stringed output for the tracks
    let mut tracks_display = String::new();
    for track in &album.tracks.items {
        tracks_display.push_str(&format!(
            "{box_v} {box_vr} [{popularity}] {name} \n",
            popularity = track
                .popularity
                .unwrap_or_else(|| panic!("track {} did not have a popularity!", track.name)),
            name = track.name,
            box_v = box_drawing_light_vertical,
            box_vr = box_drawing_light_vertical_and_right,
        ));
    }

    // Generate the stringed output for the artists
    let mut artists_display = String::new();
    for artist in &album.artists {
        artists_display.push_str(&format!(
            "{box_v} {box_vr} [{popularity}] {name} \n",
            popularity = artist
                .popularity
                .unwrap_or_else(|| panic!("artist {} did not have a popularity!", artist.name)),
            name = artist.name,
            box_v = box_drawing_light_vertical,
            box_vr = box_drawing_light_vertical_and_right,
        ));
    }

    // Generate stringed output for the album's stats
    let average_popularity = match album_stat.tracks_mean_popularity() {
        Some(p) => format!("{}", p),
        None => String::from("None"),
    };

    let popularity_dist = match album_stat.tracks_popularity_distribution() {
        Some(p) => format!("{}", p),
        None => String::from("None"),
    };

    // Finally, print all the output to standard out.
    println!(
        "\n\n{id}                                           \n\
         {box_vr} name: {name}                              \n\
         {box_vr} artists:                                  \n\
         {artists}\
         {box_vr} url: https://open.spotify.com/album/{id}  \n\
         {box_vr} popularity: {popularity}                  \n\
         {box_vr} album_type: {album_type}                  \n\
         {box_vr} release_date: {release_date}              \n\
         {box_vr} tracks and popularity:                    \n\
         {tracks}\
         {box_vr} Average Popularity: {average_popularity}  \n\
         {box_ur} Distribution: {popularity_dist}           \n\
         ",
        id = album.id,
        name = album.name,
        artists = artists_display,
        popularity = album.popularity,
        album_type = album_type,
        release_date = album.release_date,
        tracks = tracks_display,
        average_popularity = average_popularity,
        popularity_dist = popularity_dist,
        box_vr = box_drawing_light_vertical_and_right,
        box_ur = box_drawing_light_up_and_right,
    );
}

fn handle_json<T>(response: &mut reqwest::Response) -> T
where
    T: DeserializeOwned,
{
    response.json().unwrap_or_else(|error| {
        json_error_handler(&error);
        std::process::exit(1);
    })
}

fn json_error_handler(e: &reqwest::Error) {
    if e.is_serialization() {
        let serde_error = match e.get_ref() {
            None => return,
            Some(err) => err,
        };
        eprintln!("Problem parsing information {}", serde_error);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
